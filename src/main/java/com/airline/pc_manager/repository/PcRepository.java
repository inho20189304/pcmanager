package com.airline.pc_manager.repository;

import com.airline.pc_manager.entity.Pc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.*;

public interface PcRepository extends JpaRepository<Pc, Long> {

}

