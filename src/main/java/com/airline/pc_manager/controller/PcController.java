package com.airline.pc_manager.controller;

import com.airline.pc_manager.model.PcRequest;
import com.airline.pc_manager.service.PcService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pc")
public class PcController {
    private final PcService pcService;

    @PostMapping("/data")
    public String setPc(@RequestBody PcRequest pcRequest) {
        pcService.setPc(pcRequest.getPcName(), pcRequest.getPcManager());

        return "ok";
    }
}
