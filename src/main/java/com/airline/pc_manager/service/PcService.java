package com.airline.pc_manager.service;

import com.airline.pc_manager.entity.Pc;
import com.airline.pc_manager.repository.PcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcService {
    private final PcRepository pcRepository;

    public void setPc(String pcname, String pcmanager) {
        Pc adddata = new Pc();
        adddata.setPcName(pcname);
        adddata.setPcManager(pcmanager);

        pcRepository.save(adddata);


    }
}
