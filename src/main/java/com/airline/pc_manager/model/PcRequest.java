package com.airline.pc_manager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcRequest {
    private String pcName;
    private String pcManager;

}
